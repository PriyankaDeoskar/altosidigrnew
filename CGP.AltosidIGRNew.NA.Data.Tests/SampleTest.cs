using FluentAssertions;
using NSubstitute;
using NUnit.Framework;

namespace CGP.AltosidIGRNew.NA.Data.Tests
{
    public class SampleTest
    {
        [Test]
        public void ShouldHaveAllDependencies()
        {
            true.Should().BeTrue();

            var sample = Substitute.ForPartsOf<SampleClass>();
            var result = sample.TestMe("a");

            result.Should().Be("a");
            sample.Received().CallMe();
            sample.DidNotReceive().DontCallMe();
        }
    }
}
