(function () {
    var warning;
    if (document.cookie.indexOf("score_layout_mode=Final") > -1 || (
            BrainJocks !== undefined &&
            BrainJocks.EE !== undefined &&
            BrainJocks.EE.Layout !== undefined &&
            BrainJocks.EE.Layout.isEditingSharedLayout() === null
        )) {
        warning = document.createElement("div"), 
        warning.setAttribute("id", "final-layout-warning");
        warning.innerHTML = "-- WARNING: You are editing the FINAL layout --";
        document.getElementsByTagName("body")[0].appendChild(warning);
    }
}());
