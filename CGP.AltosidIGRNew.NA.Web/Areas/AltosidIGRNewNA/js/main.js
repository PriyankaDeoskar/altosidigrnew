define([
    "jquery",
    "scarf/Events/EventManager",
    "scarf/require.init",
    "matchHeight",
    "scorebootstrap"
], function ($, eventManager, requireInit) {
    requireInit()
        .done(eventManager.registrationComplete.bind(eventManager))
        .done(function () { $(".cgp-product").matchHeight(); });
});
