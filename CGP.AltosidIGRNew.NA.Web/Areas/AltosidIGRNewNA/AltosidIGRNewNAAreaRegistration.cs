using System.Web.Mvc;

namespace CGP.AltosidIGRNew.NA.Web.Areas.AltosidIGRNewNA
{
    public class AltosidIGRNewNAAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "AltosidIGRNewNA";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            // Register your MVC routes in here
        }
    }
}
