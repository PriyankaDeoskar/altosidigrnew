<#

    ABSTRACT

    The scaffolding process is used to setup a new solution for Sitecore work on a project to begin.

    Using a templated approach, the scaffolded solution is copied to a directory, then this program is executed to rename
    the various files, projects, solution artifacts, etc. to the name of the project.


    INSTRUCTIONS
    
      1.) Copy the scaffolded solution to a directory where you plan to house the project workspace

      2.) Install the required version of Sitecore into the .\sandbox folder of your solution directory - the .gitignore should be setup to ignore this folder already.
          For this example, it is assumed that the URL of the sandbox site is http://projectname

      3.) Open a command prompt with access in the path to MSBUILD and administrative permissions and run the scaffold.ps1 program with powershell.

          c:\source\projectname> powershell -file scaffold.ps1 -brand "BrandName" -marketgroup "MarketGroupName" -site "domain"

          PARAMETERS

          -brand "BrandName" - The name of your brand.

          -marketgroup "MarketGroupName" - The name of your market group root element and you area name (without spaces)

		  "CGP.BrandName.MarketGroupName" will be used as the solution name.
		  "BrandNameMarketGroupName" will be used as the web area name.

          -site "domain" - the url of the sandbox site running locally WITHOUT THE http:// on it
		  
          -vsversion "version" - visual studio version if different from 2015. Use Visual Studio notation (11 for VS 2012, 12 for VS 2013 or 14 for VS 2015)

		  -mmarket - market (default is "US")
#>

Param (
    [Parameter(Mandatory=$True)]
    [string]$brand,

    [Parameter(Mandatory=$True)]
    [string]$marketgroup,

    [Parameter(Mandatory=$False)]
    [string]$company="CGP",

    [Parameter(Mandatory=$True)]
    [string]$site,

    [Parameter(Mandatory=$False)]
    [string]$vsversion="14",

    [Parameter(Mandatory=$False)]
    [string]$mmarket="US"	
)

<#
    The ReplaceInFiles function will find files based on a $filter, look for all occurrances of a $pattern
    and replace the matching text with $replace
#>
function ReplaceInFiles($filter, $pattern, $replace) 
{
    $files = Get-ChildItem -filter $filter -exclude scaffold.ps1 -recurse | 
                Where { ($_.FullName -cnotmatch "sandbox") `
                        -and !$_.PSIsContainer `
                        -and ($_.extension -ne ".dll") `
                        -and ($_.extension -ne ".exe") }

    foreach ($file in $files) 
    {
        (Get-Content $file.FullName) -replace $pattern, $replace |  
          Out-File $file.FullName -Encoding ascii
    }
}

$ErrorActionPreference = "Stop"

Add-Type -AssemblyName System.Web

$root = Split-Path (Resolve-Path $myInvocation.MyCommand.Path)
. "$root\Automation\shared.ps1"

"Checking if script is running from a folder that contains original files"

if (!(Test-Path .\Rename.Me.Custom.Tests\))
{
    Write-Host "Cannot find original files. Execution Stopped. Please delete all folders except 'sandbox' and copy data from package if process was interrupted." -foregroundcolor "red"
    return
}

" "
"Detecting Sitecore version"
$sitecore = DetectSitecore

$step = 0

"Will now set up local environment"

$step++; "$step. Backing up Web.config. You will thank us for this one later :)"

Copy-Item .\sandbox\Website\Web.config .\sandbox\Website\Web.config.bak

$step++; "$step. Copying license.xml to support unit tests based on Sitecore.FakeDb"

Copy-Item -Force .\sandbox\Data\license.xml .\Rename.Me.Custom.Tests\

$step++; "$step. Renaming solution artifacts to CGP.$brand.$marketGroup"

"   - Renaming file names"

Get-ChildItem -filter "zzzCGP.Rename.Me.*.config" -recurse | Rename-Item -NewName { $_.name -replace "Rename\.Me","$brand.$marketGroup" }

Get-ChildItem -filter "*Rename.Me*" -recurse | Rename-Item -NewName { $_.name -replace "Rename\.Me","CGP.$brand.$marketGroup" }

"   - Renaming file contents"

ReplaceInFiles * "Rename\.Me" "CGP.$brand.$marketGroup"

"   - Renaming Area folder and Area Registration class"

Get-ChildItem -filter "*RenameMeArea*" -recurse | Rename-Item -NewName { $_.name -replace "RenameMeArea","$brand$marketgroup" }

"   - Renaming Area references"
ReplaceInFiles *.csproj RenameMeArea "$brand$marketgroup"
ReplaceInFiles *.config RenameMeArea "$brand$marketgroup"
ReplaceInFiles *.cs RenameMeArea "$brand$marketgroup"
ReplaceInFiles *.cshtml RenameMeArea "$brand$marketgroup"
ReplaceInFiles *.js RenameMeArea "$brand$marketgroup"

$step++; "$step. Renaming company name on the assembly and TDS packages to $company"

ReplaceInFiles AssemblyInfo.cs "Rename Me Company" $company
ReplaceInFiles *.scproj "Rename Me Company" "$([System.Web.HttpUtility]::HtmlEncode($company))"

$step++; "$step. Renaming local site reference to point to http://$site"

"   - Renaming TDS deployment targets"

ReplaceInFiles *.scproj http://renameme "http://$site"

"   - Renaming Sitecore <site> configuration patches for Sandbox profiles"

ReplaceInFiles *.config renameme $site
ReplaceInFiles *.config brand $brand
ReplaceInFiles *.config marketgroup $marketgroup
ReplaceInFiles *.config market $mmarket

$step++; "$step. Generating new GUIDs for solution project files and TDS connector"

$guids = @("94D68EEC-1D7C-44C8-B88D-77B13B2EB0C9",
           "3E9DE83C-1950-40EF-9FDF-4F314CCBE3E3",
           "668F676F-70FB-4683-B65F-C22DBBCD2C2A",
           "22FB2DFF-5BB5-4882-91C7-A20C046FF3AA",
           "1FD3AE5D-2764-4DAD-8AAC-8AEFB2A649D3",
           "F9A08539-4186-42FC-B9FD-A9581D9D23C5",
           "050B1C62-63D1-478F-A98F-FCFA240FE4F2",
           "5F0FDFF8-F527-4A42-8F92-F7D7A79F2303",
           "79C7C11D-293A-40B8-AFF5-40196ACEB1FE",
           "A97C277A-BCA5-4500-8021-497905DCF2D2",
           "cfa3e187-8e42-4e43-a4af-8da497648d7d")

foreach ($guid in $guids)
{
    $newguid = [guid]::NewGuid().ToString().ToUpper()

    ReplaceInFiles *.sln $guid $newguid
    ReplaceInFiles *.csproj $guid $newguid
    ReplaceInFiles *.scproj $guid $newguid
}


$step++; "$step. Setup MVC references in solution based on Sitecore release"
<# 
For Reference: 
Sitecore  Razor  MVC
========  =====  ===
   7.0      1    3.0
   7.1      2    4.0 
   7.2      3    5.1
   7.5      3    5.1
#>
" "
" "
"   - For Sitecore $($sitecore.Version) - Copy pre-built config files"
"     ==============================================================="
"   - Copying - .\Automation\Sitecore-Versions\$($sitecore.Version)\packages.config ==> .\CGP.$brand.$marketGroup.Web\"
Copy-Item -force ".\Automation\Sitecore-Versions\$($sitecore.Version)\packages.config" .\CGP.$brand.$marketGroup.Web\

"   - Copying - .\Automation\Sitecore-Versions\$($sitecore.Version)\Web.config ==> .\CGP.$brand.$marketGroup.Web\Areas\$brand$marketgroup\Views\"
Copy-Item -force ".\Automation\Sitecore-Versions\$($sitecore.Version)\Web.config" .\CGP.$brand.$marketGroup.Web\Areas\$brand$marketgroup\Views\


$step++; "$step. Copying Sitecore binary dependencies into Libs\Sitecore"

$libs = @("Lucene.Net.dll", 
          "Sitecore.Analytics.dll", 
          "Sitecore.Buckets.dll", 
          "Sitecore.Kernel.dll",
          "Sitecore.Logging.dll",
          "Sitecore.Mvc.dll",
          "Sitecore.Mvc.Analytics.dll",
          "sitecore.nexus.dll",
          "Sitecore.Update.dll",
          "Sitecore.Zip.dll")

foreach ($lib in $libs) 
{
    "   - Copying - .\sandbox\Website\bin\$lib ==> .\Libs\Sitecore"
    Copy-Item -force ".\sandbox\Website\bin\$lib" .\Libs\Sitecore\
}

$step++; "$step. Restoring NuGet packages..." 

.\.nuget\nuget.exe restore "CGP.$brand.$marketGroup.sln"

$step++; "$step. Add MVC references into CGP.$brand.$marketGroup.Web project"

(Get-Content "CGP.$brand.$marketGroup.Web\CGP.$brand.$marketGroup.Web.csproj") `
  -replace '<Reference Include="Scafolding.References.Placeholder" />', `
           (Get-Content ".\Automation\Sitecore-Versions\$($sitecore.Version)\references.xml") |  
  Out-File "CGP.$brand.$marketGroup.Web\CGP.$brand.$marketGroup.Web.csproj" -Encoding ascii

$step++; "$step. Will now build and deploy Sandbox configuration"

C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe "CGP.$brand.$marketGroup.sln" "/p:Configuration=Sandbox" "/p:VisualStudioVersion=$vsversion.0" /v:normal /nologo
C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe "CGP.$brand.$marketGroup.TDS.Master\CGP.$brand.$marketGroup.TDS.Master.scproj" "/p:VisualStudioVersion=$vsversion.0" "/p:Configuration=Sandbox" /t:Deploy /v:minimal /nologo

Write-Host "All Done." -foregroundcolor "green"
