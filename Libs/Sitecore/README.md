## Libs\Sitecore

Copy the following binaries from your Sitecore installation (`.\Website\bin`) into here:

* Lucene.Net.dll
* Sitecore.Analytics.dll
* Sitecore.Buckets.dll
* Sitecore.Kernel.dll
* Sitecore.Logging.dll
* sitecore.nexus.dll
* Sitecore.Update.dll
* Sitecore.Zip.dll
