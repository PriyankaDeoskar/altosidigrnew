Abstract
========

The scaffolding process is used to setup a new solution for Sitecore work on a project to begin.

Using a templated approach, the scaffolded solution is copied to a directory, then this program is executed to rename
the various files, projects, solution artifacts, etc. to the name of the project.
Complete instructions are in Confluence: https://centralweb.atlassian.net/wiki/spaces/DEV/pages/3702804/New+CGP+Brand+Marketing+Group+Setup


## Instructions


  1. Copy the scaffolded solution to a directory where you plan to house the project workspace
  2. Install the required version of Sitecore into the `.\sandbox` folder of your solution directory - the `.gitignore` should be setup to ignore this folder already. For this example, it is assumed that the URL of the sandbox site is `http://projectname`
  3. Open a command prompt with **access in the path to `MSBuild`** and administrative permissions and run the `scaffold.ps1` program with powershell.

```
c:\source\projectname> powershell -file scaffold.ps1 -brand "BrandName" -marketgroup "MarketGroupName" -site "domain"
```

### Parameters


* `-brand "BrandName"` - The name of your brand.
* `-marketgroup "MarketGroupName"` - The name of your market group root element and you area name (without spaces)
* `-site "domain"` - the url of the sandbox site running locally WITHOUT THE http:// on it
* `-vsversion "vsversion"` - Optional Visual Studio version (default 14).
* `-mmarket "market"` - Optional market (default US).

## Instructions continued


  4. Review CGP.brand.marketgroup.Web\packages.config file. Verify build versions. 
  5. Install packages. Open Tools --> Nuget Package Manager --> Package Manager Console. Save and build. 
  6. Add new config files. In Solution Explorer expand folder CGP.brand.marketgroup.Environments\ALL\App_Config\Include. Add Existing Items and navigate to folder CGP.brand.marketgroup.Environments\\_New_Configs. Select all config files in folder.
  7. Save, rebuild and deploy.

## Index Configuration

Solution contains configuration for two indexes 

* *.ProductSearch.config - index for product catalog
* *.SiteSearch.config - index for Site pages search

These indexes contains some sections that should be manually updated

* <indexes hint="list:AddIndex"><index id="renameme-products"> renameme should be replaced with site name
* <indexes><index><param desc="applicationId">3M4M0OI45T</param> should contain Algolia Application Id
* <indexes><index><param desc="fullApiKey">0a0bb6e3466ed4805e45826472d096e9</param> should contain Algolia Full Access API key
* <indexes><index><param desc="indexName">renameme-products-temp</param> renameme should be replaced with site name, temp should be replaced with environment key
* <indexes><index><Site>RenameMeArea</Site> should be replaced with site name as it is defined in <sites> section. Site name is used for URls generation.
* <indexes><index><locations><crawler><Root> - should point to Site Homepage for Site Index and to Products Landing Page for products index
* <indexes><index><configuration ref="contentSearch/indexConfigurations/renamemeproductsAlgoliaIndexConfiguration" /> should be renamed as well as referred section
* <indexConfigurations><renamemeproductsAlgoliaIndexConfiguration><include> - should ininclude all templates for index. Site Index - all Page templates, Products index - products details page
